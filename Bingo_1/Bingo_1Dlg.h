
// Bingo_1Dlg.h : 헤더 파일
//

#pragma once
#define MAX_BUTTON_COUNT 5

// CBingo_1Dlg 대화 상자
class CBingo_1Dlg : public CDialogEx
{
// 생성입니다.
public:
	CBingo_1Dlg(CWnd* pParent = NULL);	// 표준 생성자입니다.
	BOOL turn;
	BOOL start; // start 버튼이 눌리지 않았을 때에 , 버튼이 클릭되지 않는다.
	int file[50]; // File Load 된 data 가 저장되는 배열.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_BINGO_1_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
private:
	CButton computer_btn[MAX_BUTTON_COUNT][MAX_BUTTON_COUNT];
	CButton player_btn[MAX_BUTTON_COUNT][MAX_BUTTON_COUNT];
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedButton3();
	afx_msg void OnBnClickedButton4();
	afx_msg void OnBnClickedButton5();
	afx_msg void OnButtonClicked(UINT nIDbutton);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void CBingo_1Dlg::CheckSelectedBtn(UINT nIDbutton);
	afx_msg BOOL CBingo_1Dlg::CheckBtnContents(UINT nIDbutton);
	afx_msg int CBingo_1Dlg::CheckBingo(CButton btns[MAX_BUTTON_COUNT][MAX_BUTTON_COUNT]);
	afx_msg void CBingo_1Dlg::LoadText();
};
