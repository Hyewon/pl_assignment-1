
// Bingo_1Dlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "Bingo_1.h"
#include "Bingo_1Dlg.h"
#include "afxdialogex.h"
#include<iostream>
#include<fstream>
using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#endif
// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();
	// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CBingo_1Dlg 대화 상자



CBingo_1Dlg::CBingo_1Dlg(CWnd* pParent /*=NULL*/)
: CDialogEx(CBingo_1Dlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CBingo_1Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CBingo_1Dlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, &CBingo_1Dlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CBingo_1Dlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, &CBingo_1Dlg::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON4, &CBingo_1Dlg::OnBnClickedButton4)
	ON_BN_CLICKED(IDC_BUTTON5, &CBingo_1Dlg::OnBnClickedButton5)
	ON_CONTROL_RANGE(BN_CLICKED, 25000, 25050, OnButtonClicked)
	ON_CONTROL_RANGE(BN_CLICKED, 50000, 50050, OnButtonClicked)
	ON_WM_TIMER()
END_MESSAGE_MAP()

// CBingo_1Dlg 메시지 처리기

BOOL CBingo_1Dlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	turn = true; // true 일 경우 computer , false 일 경우 player
	start = false; //start 버튼을 누르지 않았을 때는 computer , player의 버튼이 눌리지 않아야 한다.
	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);
	srand((unsigned)time(NULL));
	CString str;
	//버튼 동적 생성
	for (int i = 0; i < MAX_BUTTON_COUNT; i++){
		for (int j = 0; j < MAX_BUTTON_COUNT; j++){
			computer_btn[i][j].Create(str, WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, CRect(80 + j * 30, 120 + i * 30, 110 + j * 30, 150 + i * 30), this, 25000 + (i * 5) + j);
			computer_btn[i][j].EnableWindow(TRUE);
			player_btn[i][j].Create(str, WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, CRect(335 + j * 30, 120 + i * 30, 365 + j * 30, 150 + i * 30), this, 50000 + (i * 5) + j);
			player_btn[i][j].EnableWindow(TRUE);
		}
	}
	if (start)
	{
		for (int i = 0; i < MAX_BUTTON_COUNT; i++)
		{
			for (int j = 0; j < MAX_BUTTON_COUNT; j++)
			{
				player_btn[i][j].EnableWindow(FALSE);
			}
		}
	}
	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CBingo_1Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다.  문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CBingo_1Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CBingo_1Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CBingo_1Dlg::OnBnClickedButton1()
{
	// New Game
	CString c_number, number, str;
	start = false;
	int player_random[25];
	int computer_random[25];

	for (int i = 0; i < 25; i++)
	{
		computer_random[i] = (rand() % 50) + 1;
		for (int j = 0; j < i; j++)
		{
			if (computer_random[j] == computer_random[i]){
				i--;
			}
		}
	}

	for (int i = 0; i < 25; i++)
	{
		player_random[i] = (rand() % 50) + 1;
		for (int j = 0; j < i; j++)
		{
			if (player_random[j] == player_random[i]){
				i--;
			}
		}
	}

	for (int index = 0; index < 25; index++)
	{
		int i = index / 5;
		int j = index % 5;

		c_number.Format(L"%d", computer_random[index]);
		computer_btn[i][j].SetWindowTextW(c_number);
	}

	for (int index = 0; index < 25; index++)
	{
		int i = index / 5;
		int j = index % 5;

		c_number.Format(L"%d", player_random[index]);
		player_btn[i][j].SetWindowTextW(c_number);
	}


}

void CBingo_1Dlg::OnBnClickedButton2()
{
	// Load
	UpdateData(true);
	start = false;
	CFileDialog dlg(true, NULL , NULL, OFN_HIDEREADONLY);
	CString filename;
	if (IDOK == dlg.DoModal()) {
		filename = dlg.GetPathName();
	}
	ifstream loadfile;
	CString str;
	char c;

	loadfile.open(filename);

	for (int i = 0; i < 50; i++)
	{
		loadfile >> file[i];
		if (loadfile.fail())
		{
			file[i] = 0;
			loadfile.clear();
			loadfile >> c;
		}
		loadfile >> c;
	}

	LoadText();
	UpdateData(false);
}

void CBingo_1Dlg::OnBnClickedButton3()
{
	// Save
	UpdateData(true);
	start = false;
	CFileDialog dlg(true, NULL, NULL , OFN_HIDEREADONLY);
	ofstream savefile;
	CString str, filename;
	int content = 0;

	if (IDOK == dlg.DoModal()) {
		filename = dlg.GetPathName();
	}
	savefile.open(filename, std::ios_base::out | std::ios_base::app);
	for (int i = 0; i < MAX_BUTTON_COUNT; i++)
	{
		for (int j = 0; j < MAX_BUTTON_COUNT; j++)
		{
			computer_btn[i][j].GetWindowText(str);
			if (!str.Compare(L""))
			{
				savefile << 'B';
				savefile << '|';
			}
			else
			{
				content = _tstoi(str);
				savefile << content;
				savefile << '|';
			}
		}
		savefile << endl;
	}

	for (int i = 0; i < MAX_BUTTON_COUNT; i++)
	{
		for (int j = 0; j < MAX_BUTTON_COUNT; j++)
		{
			player_btn[i][j].GetWindowText(str);
			if (!str.Compare(L""))
			{
				savefile << 'B';
				savefile << '|';
			}
			else
			{
				content = _tstoi(str);
				savefile << content;
				savefile << '|';
			}
		}
		savefile << endl;
	}
	UpdateData(false);
}

void CBingo_1Dlg::OnBnClickedButton4()
{
	// Start
	start = true;
	turn = true;
	if (start)
	{
		SetTimer(1, 2000, 0);

	}
}

void CBingo_1Dlg::OnBnClickedButton5()
{
	// Exit 
	CDialog::OnCancel();
}

void CBingo_1Dlg::OnButtonClicked(UINT nIDbutton)
{
	CheckSelectedBtn(nIDbutton);
}

void CBingo_1Dlg::CheckSelectedBtn(UINT nIDbutton)
{//버튼이 선택되었을 때 , 상대방 버튼과 일치하는 값이 있는지 검사하고 , 일치할 경우 contents를 "" 로 바꿔준다.
	int player_i = 0, player_j = 0;
	int computer_i = 0, computer_j = 0;
	CString str;
	CString computer, player, clicked_value;

	if (nIDbutton > 49999 && turn == false){//player 가 선택 했을 때computer의 버튼 내용과 비교해준다.
		clicked_value = "";
		player_i = (nIDbutton - 50000) / 5;
		player_j = (nIDbutton - 50000) % 5;
		player_btn[player_i][player_j].GetWindowTextW(clicked_value);
		player_btn[player_i][player_j].SetWindowTextW(L"");
		for (int i = 0; i < MAX_BUTTON_COUNT; i++)
		{
			for (int j = 0; j < MAX_BUTTON_COUNT; j++)
			{
				computer_btn[i][j].GetWindowTextW(computer);
				player_btn[i][j].GetWindowTextW(player);
				if (!clicked_value.Compare(computer))
				{//player가 클릭한 버튼의 값과 컴퓨터의 버튼의 값을 비교 한 후 같을 경우 ""로 초기화 해 준다
					computer_btn[i][j].SetWindowTextW(L"");
				}
			}
		}
		if (CheckBingo(player_btn) == 3)
		{
			AfxMessageBox(_T("Player Win!"), MB_OK);
		}
		if (CheckBingo(computer_btn) == 3)
		{
			AfxMessageBox(_T("Computer Win!"), MB_OK);
		}
		turn = true;
		SetTimer(1, 2000, 0);
	}
	else if (nIDbutton > 24999 && nIDbutton < 50000 && turn == true && start){//computer가 선택 했을 때 player의 버튼 내용과 비교해준다.
		clicked_value = "";
		computer_i = (nIDbutton - 25000) / 5;
		computer_j = (nIDbutton - 25000) % 5;
		computer_btn[computer_i][computer_j].GetWindowTextW(clicked_value);
		computer_btn[computer_i][computer_j].SetWindowTextW(L"");
		for (int i = 0; i < MAX_BUTTON_COUNT; i++)
		{
			for (int j = 0; j < MAX_BUTTON_COUNT; j++)
			{
				player_btn[i][j].GetWindowTextW(player);
				computer_btn[i][j].GetWindowTextW(computer);
				if (!clicked_value.Compare(player))
				{
					player_btn[i][j].SetWindowTextW(L"");
				}
			}
		}
		if (CheckBingo(player_btn) == 3)
		{
			AfxMessageBox(_T("Player Win!"), MB_OK);
		}
		if (CheckBingo(computer_btn) == 3)
		{
			AfxMessageBox(_T("Computer Win!"), MB_OK);
		}
		turn = false;
		KillTimer(1);
	}
}

void CBingo_1Dlg::OnTimer(UINT_PTR nIDEvent)
{
	CDialogEx::OnTimer(nIDEvent);
	int random_id, i = 0;
	CString str;
	while (turn){
		do//computer가 random으로 버튼 선택
		{
			random_id = (rand() % 25) + 25000;
			//str.Format(L"random id is %d\n", random_id);
			//OutputDebugString(str);	
			CheckSelectedBtn(random_id);
		} while (CheckBtnContents(random_id)); //선택된 버튼의 내용이 "" 인 경우 다시 선택
	}
}

BOOL CBingo_1Dlg::CheckBtnContents(UINT nIDbutton)
{//버튼의 contents 가 "" 인지 아닌지 확인해 준다.
	int player_i = 0, player_j = 0;
	int computer_i = 0, computer_j = 0;
	CString computer, player, clicked_value;

	if (nIDbutton > 49999){
		clicked_value = "";
		player_i = (nIDbutton - 50000) / 5;
		player_j = (nIDbutton - 50000) % 5;
		player_btn[player_i][player_j].GetWindowTextW(clicked_value);
	}
	else if (nIDbutton > 24999 && nIDbutton < 50000){
		clicked_value = "";
		computer_i = (nIDbutton - 25000) / 5;
		computer_j = (nIDbutton - 25000) % 5;
		computer_btn[computer_i][computer_j].GetWindowTextW(clicked_value);
	}

	if (clicked_value.Compare(L""))
	{
		return true;
	}
	else
		return false;
}

int CBingo_1Dlg::CheckBingo(CButton btns[MAX_BUTTON_COUNT][MAX_BUTTON_COUNT])
{//빙고의 갯수를 체크 , 빙고가 성립하는 경우는 가로 , 세로 , 대각선(2가지) 총 4가지의 경우이다.
	int i, j, sum = 0, bingo = 0;
	CString contents;

	//가로 빙고에 대하여 빙고 체크
	for (i = 0; i < MAX_BUTTON_COUNT; i++) {
		for (j = 0; j < MAX_BUTTON_COUNT; j++) {
			btns[i][j].GetWindowTextW(contents);
			if (!contents.Compare(L"")){
				sum++;
			}
		}
		if (sum == 5) {
			bingo++;
		}
		sum = 0;
	}

	//세로 빙고에 대하여 빙고 체크
	for (j = 0; j < MAX_BUTTON_COUNT; j++) {
		for (i = 0; i < MAX_BUTTON_COUNT; i++) {
			btns[i][j].GetWindowTextW(contents);
			if (!contents.Compare(L"")){
				sum++;
			}
		}
		if (sum == 5) {
			bingo++;
		}
		sum = 0;
	}

	//대각선 1에 대하여 빙고 체크
	for (i = 0; i < MAX_BUTTON_COUNT; i++) {
		btns[i][i].GetWindowTextW(contents);
		if (!contents.Compare(L"")){
			sum++;
		}
		if (sum == 5) {
			bingo++;
		}
		sum = 0;
	}

	//대각선 2에 대하여 빙고 체크
	for (i = 0; i < MAX_BUTTON_COUNT; i++) {
		for (j = 4; j <= 0; j--) {
			btns[i][j].GetWindowTextW(contents);
			if (!contents.Compare(L"")){
				sum++;
			}
		}
		if (sum == 5) {
			bingo++;
		}
		sum = 0;
	}

	return bingo;
}

void CBingo_1Dlg::LoadText()
{//Load를 통해 저장된 file[index]의 값들을 각 해당하는 버튼에 입력해줌.
	start = false;
	CString content;
	int index = 0;

	for (int i = 0; i < MAX_BUTTON_COUNT; i++)
	{
		for (int j = 0; j < MAX_BUTTON_COUNT; j++)
		{
			if (file[index] == 0)
			{
				computer_btn[i][j].SetWindowTextW(L"");
				index++;
			}
			else
			{
				content.Format(L"%d", file[index]);
				computer_btn[i][j].SetWindowTextW(content);
				//OutputDebugString(content);
				index++;
			}
		}
	}

	index = 25;
	for (int i = 0; i < MAX_BUTTON_COUNT; i++)
	{
		for (int j = 0; j < MAX_BUTTON_COUNT; j++)
		{
			if (file[index] == 0)
			{
				player_btn[i][j].SetWindowTextW(L"");
				index++;
			}
			else
			{
				content.Format(L"%d", file[index]);
				player_btn[i][j].SetWindowTextW(content);
				//OutputDebugString(content);
				index++;
			}
		}
	}
}